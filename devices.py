from common import ShellyMQTTDevice
import paho.mqtt.client as mqtt

bigmac_mqtt = mqtt.Client()

def connect():
  bigmac_mqtt.connect("192.168.178.33", 1883, 60)

alwin_regal_hinten_rgb = ShellyMQTTDevice(bigmac_mqtt, "shellies/shelly-alwin-regal-rgb/color/0/set")
alwin_desk_rgb = ShellyMQTTDevice(bigmac_mqtt, "shellies/alwin-desk-rgb/color/0/set")

devices = [alwin_regal_hinten_rgb, alwin_desk_rgb]

def set_all_colors(r: int, g: int, b: int):
  for device in devices:
    device.set_color(r, g, b)

def fade_all_threaded(from_rgb: tuple, to_rgb: tuple, delay: float, step_size=1):
  for device in devices:
      threading.Thread(target=device.fade, args=((from_rgb, to_rgb, delay)), kwargs={"step_size": step_size}).start()

