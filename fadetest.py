from devices import connect, set_all_colors, devices
import time
import threading
connect()

def deviceloop(device):
  while True:
    colors = [(255, 0, 0), (0, 255, 0), (0, 0, 255)]
    last_color = colors[-1]
    for c in colors:
      device.fade(last_color, c, 0.1, step_size=10)
      time.sleep(0.2)
      last_color = c


def main():
  for i in devices:
      print(i)
      threading.Thread(target=deviceloop, args=((i,))).start()
      #i.fade((255, 0, 0), (0, 0, 255), 0.01)
      #i.fade((0, 0, 255), (255, 0, 0), 0.01)



main()
