import time

def strobe(devices, duration, cycles=None, color=None):
  if not color:
    color = (255, 255, 255)
  if not cycles:
    cycles = duration * 6
  delay = duration / cycles
  for i in range(1, cycles):
    time.sleep(delay)
    for device in devices:
      if i % 2 == 0:
        device.set_color(color[0], color[1], color[2])
      else:
        device.set_color(0,0,0)


