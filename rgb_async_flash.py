import paho.mqtt.client as mqtt
import time

client = mqtt.Client()
client.connect("localhost",1883,60)

# fetch all items

devices = ["shellies/alwin-desk-rgb/color/0/set", "shellies/shelly-alwin-regal-rgb/color/0/set"]


#devices.append(openhab.get_item('alwin_unendlichkeitsspiegel_color'))
#devices.append(openhab.get_item('alwindeskrgb_Farbe'))

current_device = 0

colors = [
        '{"red": 255, "green": 0, "blue": 0}',
        '{"red": 0, "green": 255, "blue": 0}',
        '{"red": 0, "green": 0, "blue": 255}'
]

while True:
  for i in colors:
    client.publish(devices[current_device], i)
    if current_device >= len(devices) - 1:
      current_device = 0
    else:
      current_device += 1
    time.sleep(0.3)

