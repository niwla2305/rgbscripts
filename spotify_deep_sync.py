
from devices import connect, set_all_colors, devices
import time
import json
import spotipy
from spotipy.oauth2 import SpotifyOAuth
from dotenv import load_dotenv
import os

load_dotenv()
connect()

client_id = os.environ["SPOTIFY_CLIENT_ID"]
secret = os.environ["SPOTIFY_CLIENT_SECRET"]

sp = spotipy.Spotify(auth_manager=SpotifyOAuth(client_id=client_id,
                                               client_secret=secret,
                                               redirect_uri="http://localhost:45432",
                                               scope="user-read-playback-state"))



colors = [(255, 0, 0), (0, 255, 0), (0, 0, 255), (255, 255, 0), (0, 255, 255), (255, 0, 255)]

while True:
  print("fetching new song...")
  results = sp.current_playback()
  time_fetched = time.time()
  try:
    song_id = results["item"]["id"]
    print(f"got song: {results['item']['name']}")
  except TypeError:
    time.sleep(3)
    continue
  progress = results["progress_ms"] / 1000

  song = sp.audio_analysis(song_id)

  slept = False
  next_color_index = 0
  for beat in song["beats"]:
      if beat["start"] < progress:
          continue
      else:
          if not slept:
            time.sleep(beat["start"] - progress + (time.time() - time_fetched))
            slept = True

          color = colors[next_color_index]
          if next_color_index <= len(colors) - 2:
            next_color_index += 1
          else:
            next_color_index = 0
          set_all_colors(color[0], color[1], color[2])
          time.sleep(beat["duration"])
