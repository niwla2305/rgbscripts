import json
import time

class Device:
    def set_color(self, r: int, g: int, b: int):
        raise NotImplementedError
    
    def fade(self, from_rgb: tuple, to_rgb: tuple, delay: float, step_size=1):
        current_rgb = list(from_rgb)
        while current_rgb != list(to_rgb):
          print(current_rgb, to_rgb)
          for index, ch in enumerate(current_rgb):
            if ch >= to_rgb[index]:
              current_rgb[index] -= step_size
              if current_rgb[index] < 0:
                current_rgb[index] = 0
            else:
              current_rgb[index] += step_size
              if current_rgb[index] > 255:
                current_rgb[index] = 255
          self.set_color(current_rgb[0], current_rgb[1], current_rgb[2])
          time.sleep(delay)

class MQTTDevice(Device):
    def __init__(self, server, topic):
        self.server = server
        self.topic = topic

class ShellyMQTTDevice(MQTTDevice):
    def set_color(self, r: int, g: int, b: int):
        #print("setting color to: ", r,g,b)
        color_formatted = json.dumps({"red": r, "green": g, "blue": b})
        self.server.publish(self.topic, color_formatted)
