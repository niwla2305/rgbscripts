# rgbscripts

some hacks for the spotify api to sync RGB with music.
this will not work out of the box, you need to actually go through the code and adapt it for your needs.
In common.py device types are registered
Define a device in devices.py

copy `.env.example` to .env and change the values.

Spotify Oauth tokens can be obtained from https://developer.spotify.com/dashboard/

execute `spotify_deep_sync.py` to do the fancy beat to rgb sync.


This project is not affiliated or endorsed by Spotify Inc.
See LICENSE for licensing information.

