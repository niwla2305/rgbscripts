from devices import connect, set_all_colors
import time
connect()

while True:
  for value in range(1, 255):
    print(value)
    set_all_colors(value, 0, 0)
    time.sleep(0.01)
  time.sleep(1)
  set_all_colors(0,0,0)
  time.sleep(1)
