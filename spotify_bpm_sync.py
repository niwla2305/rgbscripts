
from devices import connect, set_all_colors, devices
import time
import threading
connect()

tempo = 231.04112 #/ 60 # beats per second
#tempo = 23 / 60
time_signature = 4

time_per_beat = 60 / tempo

colors = [(255, 0, 0), (0, 255, 0), (0, 0, 255), (255, 0, 255)]
#colors = [(255, 0, 0), (0, 255,0 )]


delay = tempo / time_signature

while True:
  for color in colors:
    set_all_colors(color[0], color[1], color[2])
    time.sleep(time_per_beat)
