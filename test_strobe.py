#import effects.strobe.strobe as strobe
from effects.strobe import strobe
from devices import devices, connect
import sys

connect()

color_input = sys.argv[1].split(",")
color = (int(color_input[0]), int(color_input[1]), int(color_input[2]))


strobe(devices, int(sys.argv[2]), color=color)
